//
//  UIViewController.swift
//  WeatherApp
//
//  Created by Christina on 9/8/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: Static Properties
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
}
