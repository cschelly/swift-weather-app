//
//  UIStoryboard.swift
//  WeatherApp
//
//  Created by Christina on 9/8/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    // MARK: Static Properties
    
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
