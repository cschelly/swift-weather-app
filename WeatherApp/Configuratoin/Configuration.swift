//
//  Configuration.swift
//  WeatherApp
//
//  Created by Christina on 9/9/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

enum Defaults {
    
    static let location = Location(latitude: 37.8267, longitude: -122.4233)

}

enum Configuration {
    static var refreshThreshold: TimeInterval {
        #if DEBUG
        return 60.0
        #else
        return 10.0 * 60.0
        #endif
    }
}

enum WeatherService {
    private static let apiKey = "b62e3b3b7f485bc0b07ad47ae42b872d"
    private static let baseUrl = URL(string: "https://api.darksky.net/forecast/")!
    static var authenticatedBaseUrl: URL {
        return baseUrl.appendingPathComponent(apiKey)
    }
}
