//
//  RootViewModel.swift
//  WeatherApp
//
//  Created by Christina on 9/9/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

class RootViewModel: NSObject {

    enum WeatherDataError: Error {
        case notAuthorizedToRequestLocation
        case failedToRequestLocation
        case noWeatherDataAvailable
    }

    enum WeatherDataResult {
        case success(WeatherData)
        case failure(WeatherDataError)
    }

    typealias FetchWeatherDataCompletion = (WeatherDataResult) -> Void

    var didFetchWeatherData: FetchWeatherDataCompletion?

    private let locationService: LocationService
    private let networkService: NetworkService

    init(locationService: LocationService, networkService: NetworkService ) {
        self.locationService = locationService
        self.networkService = networkService
        super.init()
        setupNotificationHandling()
    }

    private func fetchLocation() {
        locationService.fetchLocation { [weak self] (result) in
            switch result {
            case .success(let location):
                self?.fetchWeatherData(for: location)
            case .failure(let error):
                let result: WeatherDataResult = .failure(.notAuthorizedToRequestLocation)
                print("Unalbe to request location: \(error)")
                self?.didFetchWeatherData?(result)
            }
        }
    }

    private func fetchWeatherData(for location: Location) {

        let weatherRequest = WeatherRequest(baseUrl: WeatherService.authenticatedBaseUrl, location: location)
        
        networkService.fetchData(with: weatherRequest.url) { [weak self] (data, response, error) in
            if let response = response as? HTTPURLResponse {
                print("Status Code: \(response.statusCode)")
            }

            // Fetch weather data on main thread
            DispatchQueue.main.async {
                if let error = error {
                    let result: WeatherDataResult = .failure(.noWeatherDataAvailable)
                    print("Unable to fetch weather data: \(error)")
                    self?.didFetchWeatherData?(result)
                } else if let data = data {
                    let decoder = JSONDecoder()
                    // set the decoder to convert from the expected timestamp
                    decoder.dateDecodingStrategy = .secondsSince1970
                    do {
                        let darkSkyResponse = try decoder.decode(DarkSkyResponse.self, from: data)
                        let result: WeatherDataResult = .success(darkSkyResponse)
                        UserDefaults.didFetchWeatherData = Date()
                        self?.didFetchWeatherData?(result)
                    } catch {
                        let result: WeatherDataResult = .failure(.noWeatherDataAvailable)
                        print("Unable to decode JSON response: \(error)")
                        self?.didFetchWeatherData?(result)
                    }
                } else {
                    let result: WeatherDataResult = .failure(.noWeatherDataAvailable)
                    self?.didFetchWeatherData?(result)
                }
            }

        }
    }

    private func setupNotificationHandling() {
        NotificationCenter.default.addObserver(forName: Notification.Name.UIApplicationWillEnterForeground,
                                               object: nil,
                                               queue: OperationQueue.main) { [weak self] (_) in
            guard let didFetchWeatherData = UserDefaults.didFetchWeatherData else {
                self?.refresh()
                return
            }
            if Date().timeIntervalSince(didFetchWeatherData) > Configuration.refreshThreshold {
                self?.refresh()
            }
        }
    }

    func refresh() {
        fetchLocation()
    }

}

extension UserDefaults {

    private enum Keys {
        static let didFetchWeatherData = "didFetchWeatherData"
    }

    fileprivate class var didFetchWeatherData: Date? {
        get {
            return UserDefaults.standard.object(forKey: Keys.didFetchWeatherData) as? Date
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: Keys.didFetchWeatherData)
        }
    }

}


