//
//  RootViewController.swift
//  WeatherApp
//
//  Created by Christina on 9/8/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

final class RootViewController: UIViewController {

    private enum AlertType {
        case noWeatherDataAvailable
        case failedToRequestLocation
        case notAuthorizedToRequestLocation
    }
    
    // MARK: Properties
    
    var viewModel: RootViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }
            setupViewModel(with: viewModel)
        }
    }
    
    private let dayViewController: DayViewController = {
       guard let dayViewController = UIStoryboard.main.instantiateViewController(withIdentifier: DayViewController.storyboardIdentifier) as? DayViewController else {
           fatalError("Could not instantiate DayViewController")
        }
        
        // Configure day view controller to only use auto layout
        dayViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        return dayViewController
    }()
    
    private lazy var weekViewController: WeekViewController = {
        guard let weekViewController = UIStoryboard.main.instantiateViewController(withIdentifier: WeekViewController.storyboardIdentifier) as? WeekViewController else {
            fatalError("Could not instantiate WeekViewController")
        }
        
        weekViewController.delegate = self
        // Configure week view controller to only use auto layout
        weekViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        return weekViewController
    }()
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up child view controllers
        setupChildViewControllers()
        viewModel?.refresh()
    }

    private func setupChildViewControllers() {
        addChildViewController(dayViewController)
        addChildViewController(weekViewController)

        view.addSubview(dayViewController.view)
        view.addSubview(weekViewController.view)
        
        dayViewController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dayViewController.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        dayViewController.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        weekViewController.view.topAnchor.constraint(equalTo: dayViewController.view.bottomAnchor).isActive = true
        weekViewController.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        weekViewController.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        weekViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        dayViewController.didMove(toParentViewController: self)
        weekViewController.didMove(toParentViewController: self)
    }
    
    private func setupViewModel(with: RootViewModel) {
        viewModel?.didFetchWeatherData = { [weak self] (result) in
            switch result {
            case .success(let weatherData):
                let dayViewModel = DayViewModel(weatherData: weatherData.current)
                self?.dayViewController.viewModel = dayViewModel
                let weekViewModel = WeekViewModel(weatherData: weatherData.forecast)
                self?.weekViewController.viewModel = weekViewModel
            case .failure(let error):
                let alertType: AlertType
                switch error {
                case .notAuthorizedToRequestLocation:
                    alertType = .notAuthorizedToRequestLocation
                case .failedToRequestLocation:
                    alertType = .failedToRequestLocation
                case .noWeatherDataAvailable:
                    alertType = .noWeatherDataAvailable
                }
                self?.presentAlert(of: alertType)
                self?.dayViewController.viewModel = nil
                self?.weekViewController.viewModel = nil
            }
        }
    }
    
    private func presentAlert(of alertType: AlertType) {
        var title: String
        var message: String
        switch alertType {
        case .notAuthorizedToRequestLocation:
            title = "Unable to fetch weather data for your location"
            message = "Rainstorm is not authorized to access your current location. That means the application cannot present weather data."
        case .failedToRequestLocation:
            title = "Application failed to request weather data"
            message = "Something went wrong and we were not able to retrieve weather data for your location. Please relaunch and try again."
        case .noWeatherDataAvailable:
            title = "Unable to fetch weatehr data"
            message = "The application is unable to fetch weather data. Please ensure your device is connected to Wi-Fi or cellular."
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
}


extension RootViewController: WeekViewControllerDelegate {
    func controllerDidRefresh(_ controller: WeekViewController) {
        viewModel?.refresh()
    }
}
