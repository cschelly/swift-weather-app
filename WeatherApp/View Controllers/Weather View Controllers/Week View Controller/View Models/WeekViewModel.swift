//
//  WeekViewModel.swift
//  WeatherApp
//
//  Created by Christina on 9/12/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

struct WeekViewModel {
    
    let weatherData: [ForecastWeatherConditions]
    
    var numberOfDays: Int {
        return weatherData.count
    }
    
    func viewModel(for index: Int) -> WeekDayViewModel {
        return WeekDayViewModel(weatherData: weatherData[index])
    }
    
}
