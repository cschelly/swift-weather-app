//
//  WeekViewController.swift
//  WeatherApp
//
//  Created by Christina on 9/8/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

protocol WeekViewControllerDelegate: class {
    func controllerDidRefresh(_ controller: WeekViewController)
}

final class WeekViewController: UIViewController {
    
    weak var delegate: WeekViewControllerDelegate?
    
    var viewModel: WeekViewModel? {
        didSet {
            refreshControl.endRefreshing()
            if let viewModel = viewModel  {
                setupViewModel(with: viewModel)
            }
        }
    }
    
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.isHidden = true
            tableView.dataSource = self
            tableView.separatorInset = .zero
            tableView.estimatedRowHeight = 44.0
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.showsVerticalScrollIndicator = false
            tableView.refreshControl = refreshControl
        }
    }
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView! {
        didSet {
            activityIndicator.startAnimating()
            activityIndicator.hidesWhenStopped = true
        }
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.Rainstorm.baseTintColor
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Set up view
        setupView()
    }
    
    // MARK: Helper Methods
    
    private func setupView() {
        view.backgroundColor = .white
    }
    
    @objc private func refresh(_ sender: UIRefreshControl) {
        delegate?.controllerDidRefresh(self)
    }

    private func setupViewModel(with viewModel: WeekViewModel) {
        activityIndicator.stopAnimating()
        tableView.reloadData()
        tableView.isHidden = false
    }
}

extension WeekViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfDays ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeekViewTableViewCell.reuseIdentifier, for: indexPath) as? WeekViewTableViewCell else {
            fatalError("Unable to Dequeue Week View Table View Cell")
        }
        guard let viewModel = viewModel else {
            fatalError("No View Model Present")
        }
        cell.configure(with: viewModel.viewModel(for: indexPath.row))
        return cell
    }
}
