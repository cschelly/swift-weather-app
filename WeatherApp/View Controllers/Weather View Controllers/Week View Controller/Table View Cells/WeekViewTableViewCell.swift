//
//  WeekViewTableViewCell.swift
//  WeatherApp
//
//  Created by Christina on 9/16/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class WeekViewTableViewCell: UITableViewCell {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    @IBOutlet var dayLabel: UILabel? {
        didSet {
            dayLabel?.textColor = UIColor.Rainstorm.baseTextColor
            dayLabel?.font =  UIFont.Rainstorm.heavyLarge
        }
    }
    
    @IBOutlet var dateLabel: UILabel? {
        didSet {
            dateLabel?.textColor = .black
            dateLabel?.font =  UIFont.Rainstorm.lightLarge
        }
    }
    
    @IBOutlet var windSpeedLabel: UILabel? {
        didSet {
            windSpeedLabel?.textColor = .black
            windSpeedLabel?.font =  UIFont.Rainstorm.lightSmall
        }
    }
    
    @IBOutlet var temperatureLabel: UILabel? {
        didSet {
            temperatureLabel?.textColor = .black
            temperatureLabel?.font =  UIFont.Rainstorm.lightSmall
        }
    }
    
    @IBOutlet var iconImageView: UIImageView? {
        didSet {
            iconImageView?.contentMode = .scaleAspectFit
            iconImageView?.tintColor = UIColor.Rainstorm.baseTintColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(with representable: WeekDayRepresentable) {
        dayLabel?.text = representable.day
        dateLabel?.text = representable.date
        windSpeedLabel?.text = representable.windSpeed
        temperatureLabel?.text = representable.temperature
        iconImageView?.image = representable.image
    }

}
