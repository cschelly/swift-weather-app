//
//  WeekDayRepresentable.swift
//  WeatherApp
//
//  Created by Christina on 9/20/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

protocol WeekDayRepresentable {
    
    var day: String { get }
    var date: String { get }
    var temperature: String { get }
    var windSpeed: String { get }
    var image: UIImage? { get }
    
}
