//
//  NewtorkManager.swift
//  WeatherApp
//
//  Created by Christina Sund on 6/27/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import Foundation

class NetworkManager: NetworkService {
    
    func fetchData(with url: URL, completionHandler: @escaping NetworkService.FetchDataCompletion) {
        URLSession.shared.dataTask(with: url, completionHandler: completionHandler).resume()
    }
    
}
