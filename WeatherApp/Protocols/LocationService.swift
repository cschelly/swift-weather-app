//
//  LocationService.swift
//  WeatherApp
//
//  Created by Christina on 11/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

enum LocationServiceError {
    case notAuthorizedToRequestLocation
}

enum LocationServiceResult {
    case success(Location)
    case failure(LocationServiceError)
}

protocol LocationService {
    
    typealias FetchLocationCompletion = (LocationServiceResult) -> Void
    
    func fetchLocation(completion: @escaping FetchLocationCompletion)
    
}
