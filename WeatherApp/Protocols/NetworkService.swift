//
//  NetworkService.swift
//  WeatherApp
//
//  Created by Christina Sund on 6/27/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import Foundation

protocol NetworkService {
    typealias FetchDataCompletion = (Data?, URLResponse?, Error?) -> Void
    
    func fetchData(with url: URL, completionHandler: @escaping NetworkService.FetchDataCompletion)
}
