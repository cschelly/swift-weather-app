//
//  Location.swift
//  WeatherApp
//
//  Created by Christina on 11/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

struct Location {
    
    let latitude: Double
    let longitude: Double

}
