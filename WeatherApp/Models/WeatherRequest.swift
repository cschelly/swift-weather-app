//
//  WeatherRequest.swift
//  WeatherApp
//
//  Created by Christina on 9/9/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

struct WeatherRequest {
    
    let baseUrl: URL
    
    let location: Location
    
    private var latitude: Double {
        return location.latitude
    }
    
    private var longitude: Double {
        return location.longitude
    }
    
    var url: URL {
        return baseUrl.appendingPathComponent("\(latitude),\(longitude)")
    }
    
}
