//
//  XCTestCase.swift
//  WeatherAppTests
//
//  Created by Christina on 9/16/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func loadStubFromFile(name: String, extension: String) -> Data {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: `extension`)
        return try! Data(contentsOf: url!)
    }
    
}
