//
//  MockNetworkService.swift
//  WeatherAppTests
//
//  Created by Christina Sund on 6/27/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import Foundation
@testable import WeatherApp

class MockNetworkService: NetworkService {
    
    var data: Data?
    var error: Error?
    var statusCode: Int = 200
    
    func fetchData(with url: URL, completionHandler: @escaping NetworkService.FetchDataCompletion) {
        let response = HTTPURLResponse(url: url, statusCode: statusCode, httpVersion: nil, headerFields: nil)
        completionHandler(data, response, error)
    }
    
}
