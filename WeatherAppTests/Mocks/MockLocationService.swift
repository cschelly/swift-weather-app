//
//  MockLocationService.swift
//  WeatherAppTests
//
//  Created by Christina Sund on 6/26/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import Foundation
@testable import WeatherApp

class MockLocationService: LocationService {
    
    var location: Location? = Location(latitude: 0.0, longitude: 0.0)
    var delay: TimeInterval = 0.0
    
    func fetchLocation(completion: @escaping LocationService.FetchLocationCompletion) {
        var result: LocationServiceResult
        
        if let location = location {
            result = .success(location)
        } else {
            result = .failure(.notAuthorizedToRequestLocation)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            completion(result)
        }
        
    }
    
}
