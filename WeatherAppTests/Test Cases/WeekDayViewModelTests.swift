//
//  WeekDayViewModelTests.swift
//  WeatherAppTests
//
//  Created by Christina on 9/23/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeekDayViewModelTests: XCTestCase {
    
    var viewModel: WeekDayViewModel!
    
    override func setUp() {
        super.setUp()
        let data = loadStubFromFile(name: "darksky", extension: "json")
        let decoder = JSONDecoder()
        let darkSkyResponse = try! decoder.decode(DarkSkyResponse.self, from: data)
        viewModel = WeekDayViewModel(weatherData: darkSkyResponse.forecast[5])
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDate() {
        XCTAssertEqual(viewModel.date, "September 21")
    }
    
    func testTemperature() {
        XCTAssertEqual(viewModel.temperature, "54.2 °F - 54.2 °F")
    }
    
    func testWindSpeed() {
        XCTAssertEqual(viewModel.windSpeed, "4 MPH")
    }
    
    func testImage() {
        let viewModelImage = viewModel.image
        let imageDataViewModel = UIImagePNGRepresentation(viewModelImage!)
        let imageDataReference = UIImagePNGRepresentation(UIImage(named: "clear-night")!)
        
        XCTAssertNotNil(viewModelImage)
        XCTAssertEqual(viewModelImage!.size.width, 45.0)
        XCTAssertEqual(viewModelImage!.size.height, 33.0)
        //XCTAssertEqual(imageDataViewModel, imageDataReference)
    }
    
}
