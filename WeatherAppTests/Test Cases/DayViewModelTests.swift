//
//  DayViewModelTests.swift
//  WeatherAppTests
//
//  Created by Christina on 9/16/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import XCTest
@testable import WeatherApp

class DayViewModelTests: XCTestCase {
    
    var viewModel: DayViewModel!
    
    override func setUp() {
        super.setUp()
        
        let data = loadStubFromFile(name: "darksky", extension: "json")
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        let darkskyResponse = try! decoder.decode(DarkSkyResponse.self, from: data)
        viewModel = DayViewModel(weatherData: darkskyResponse.current)
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDate() {
        XCTAssertEqual(viewModel.date, "Sun, September 16 2018")
    }
    
    func testTime() {
        XCTAssertEqual(viewModel.time, "11:17 AM")
    }
    
    func testSummary() {
        XCTAssertEqual(viewModel.summary, "Clear")
    }
    
    func testTemperature() {
        XCTAssertEqual(viewModel.temperature, "64.0 °F")
    }
    
    func testWindSpeed() {
        XCTAssertEqual(viewModel.windSpeed, "4 MPH")
    }
    
    func testImage() {
        let viewModelImage = viewModel.image
        let imageDataViewModel = UIImagePNGRepresentation(viewModelImage!)
        let imageDataReference = UIImagePNGRepresentation(UIImage(named: "clear-day")!)
        
        XCTAssertNotNil(viewModelImage)
        XCTAssertEqual(viewModelImage!.size.width, 48.0)
        XCTAssertEqual(viewModelImage!.size.height, 48.0)
        XCTAssertEqual(imageDataViewModel, imageDataReference)
    }
    
    
}
