//
//  RootViewModelTests.swift
//  WeatherAppTests
//
//  Created by Christina Sund on 6/26/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import XCTest
import Mockingjay
@testable import WeatherApp

class RootViewModelTests: XCTestCase {

    var viewModel: RootViewModel!
    var mockLocationService: MockLocationService!
    
    override func setUp() {
        mockLocationService = MockLocationService()
        viewModel = RootViewModel(locationService: mockLocationService, networkService: NetworkManager())
    }

    override func tearDown() {
        UserDefaults.standard.removeObject(forKey: "didFetchWeatherData")
    }

    func testRefresh_Success() {
        let data = loadStubFromFile(name: "darksky", extension: "json")
        stub(everything, jsonData(data))
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { result in
            if case .success(let weatherData) = result {
                XCTAssertEqual(weatherData.latitude, 37.8267)
                XCTAssertEqual(weatherData.longitude, -122.4233)
                expectation.fulfill()
            }
        }
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testRefresh_FailedToFetch() {
        mockLocationService.location = nil
        let expectation = XCTestExpectation(description: "Fetch Weather Data Fails")
        viewModel.didFetchWeatherData = { result in
            if case .failure(let error) = result {
                XCTAssertEqual(error, RootViewModel.WeatherDataError.notAuthorizedToRequestLocation)
                expectation.fulfill()
            }
        }
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testRefresh_FailedToFetchWeatherData_RequestFailed() {
        let error = NSError(domain: "com.weatherapp.network", code: 1, userInfo: nil)
        stub(everything, failure(error))
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { result in
            if case .failure(let error) = result {
                XCTAssertEqual(error, RootViewModel.WeatherDataError.noWeatherDataAvailable)
                expectation.fulfill()
            }
        }
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testRefresh_FailedToFetchWeatherData_InvalidResponse() {
        let body = ["some": "body"]
        stub(everything, json(body))
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { result in
            if case .failure(let error) = result {
                XCTAssertEqual(error, RootViewModel.WeatherDataError.noWeatherDataAvailable)
                expectation.fulfill()
            }
        }
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testRefresh_FailedToFetchWeatherData_NoErrorNoResponse() {
        stub(everything) { request -> Response in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return .success(response, .noContent)
            
        }
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { result in
            if case .failure(let error) = result {
                XCTAssertEqual(error, RootViewModel.WeatherDataError.noWeatherDataAvailable)
                expectation.fulfill()
            }
        }
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testApplicationWillEnterForeground_NoTimestamp() {
        UserDefaults.standard.removeObject(forKey: "didFetchWeatherData")
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { _ in
                expectation.fulfill()
        }
        
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testApplicationWillEnterForeground_ShouldRefresh() {
        UserDefaults.standard.set(Date().addingTimeInterval(-3600.0), forKey: "didFetchWeatherData")
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        viewModel.didFetchWeatherData = { _ in
            expectation.fulfill()
        }
        
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testApplicationWillEnterForeground_ShouldNotRefresh() {
        UserDefaults.standard.set(Date(), forKey: "didFetchWeatherData")
        let expectation = XCTestExpectation(description: "Fetch Weather Data")
        expectation.isInverted = true
        viewModel.didFetchWeatherData = { _ in
            expectation.fulfill()
        }
        
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        viewModel.refresh()
        wait(for: [expectation], timeout: 3.0)
    }
    
}
