//
//  WeekViewModelTests.swift
//  WeatherAppTests
//
//  Created by Christina on 9/23/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeekViewModelTests: XCTestCase {
    
    var viewModel: WeekViewModel!
    
    override func setUp() {
        super.setUp()
        let data = loadStubFromFile(name: "darksky", extension: "json")
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        let darkSkyResponse = try! decoder.decode(DarkSkyResponse.self, from: data)
        viewModel = WeekViewModel(weatherData: darkSkyResponse.forecast)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testNumberOfDays() {
        XCTAssert(viewModel.numberOfDays == 8)
    }
    
    func testViewModelFor() {
        let weekViewModel = viewModel.viewModel(for: 5)
        XCTAssert(weekViewModel.day == "Sunday")
        XCTAssert(weekViewModel.date == "September, 21 2018")
    }
    
}
